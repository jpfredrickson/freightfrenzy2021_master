package org.firstinspires.ftc.teamcode.hardware;

import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.hardware.Servo;

public class ShootingSystem {
    private DcMotor liftMotor1; // ShooterMotor, IntakeMotor, lockingMotor;
    //private CRServo flappyServo;
    private CRServo wheelServo;
    private Servo bucketServo;


    public ShootingSystem(HardwareMap hardwareMap) {

        //Servo Motors
        wheelServo = hardwareMap.get(CRServo.class, "wheelservo");
        bucketServo = hardwareMap.get(Servo.class, "bucketservo");

        //DC Motors
        liftMotor1 = hardwareMap.get(DcMotor.class, "liftMotor");
        liftMotor1.setDirection(DcMotorSimple.Direction.REVERSE);
        liftMotor1.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        liftMotor1.setTargetPosition(0);
        liftMotor1.setMode(DcMotor.RunMode.RUN_TO_POSITION);

        //OLD CODE FOR AN EXAMPLE
        //IntakeMotor = hardwareMap.get(DcMotor.class, "intakeMotor");
        //IntakeMotor.setDirection(DcMotorSimple.Direction.REVERSE);
        //IntakeMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        //IntakeMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        //IntakeMotor.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        //IntakeMotor.setTargetPosition(0);
    }


    public void level0() {
        liftMotor1.setPower(1.0);
        liftMotor1.setTargetPosition(40);
    }

    public void level1() {
        liftMotor1.setPower(1.0);
        liftMotor1.setTargetPosition(185);
    }

    public void level2() {
        liftMotor1.setPower(1.0);
        liftMotor1.setTargetPosition(350);
        //IntakeMotor.setTargetPosition(IntakeMotor.getCurrentPosition() + 495);
        //IntakeMotor.setTargetPosition(IntakeMotor.getCurrentPosition() + 600);
        //IntakeMotor.setPower(p1);}
    }

    public void level4() {
        liftMotor1.setPower(1.0);
        liftMotor1.setTargetPosition(0);
    }

    public void Load() {
        bucketServo.setPosition(0.7);
    }

    public void UnLoad() {
        bucketServo.setPosition(0);
    }

    public void autoFireRings() {
        wheelServo.setDirection(CRServo.Direction.FORWARD);
        wheelServo.setPower(0.3);}

     public void autoFireRingsrev(){
        wheelServo.setDirection(CRServo.Direction.REVERSE);
        wheelServo.setPower(0.3);
    }

    public void stopFiring() {
        wheelServo.setPower(0);
    }


}