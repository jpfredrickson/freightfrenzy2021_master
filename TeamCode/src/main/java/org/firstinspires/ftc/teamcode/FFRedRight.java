/*
 * Copyright (c) 2020 OpenFTC Team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package org.firstinspires.ftc.teamcode;

import com.acmerobotics.roadrunner.geometry.Pose2d;
import com.acmerobotics.roadrunner.trajectory.Trajectory;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

import org.firstinspires.ftc.robotcore.external.hardware.camera.WebcamName;
import org.firstinspires.ftc.teamcode.drive.SampleMecanumDrive;
import org.firstinspires.ftc.teamcode.hardware.ShootingSystem;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import org.openftc.easyopencv.OpenCvCamera;
import org.openftc.easyopencv.OpenCvCameraFactory;
import org.openftc.easyopencv.OpenCvCameraRotation;
import org.openftc.easyopencv.OpenCvPipeline;

@com.qualcomm.robotcore.eventloop.opmode.Autonomous
public class FFRedRight extends LinearOpMode {
    //OpenCvInternalCamera webcam;
    //VisionTest11 pipeline;

    OpenCvCamera webcam;
    VisionTestBB pipeline;

    @Override
    public void runOpMode() {
        SampleMecanumDrive drive = new SampleMecanumDrive(hardwareMap);
        ShootingSystem shootingSystem = new ShootingSystem(hardwareMap);

        int LevelNumber;

        int cameraMonitorViewId = hardwareMap.appContext.getResources().getIdentifier("cameraMonitorViewId", "id", hardwareMap.appContext.getPackageName());
        //webcam = OpenCvCameraFactory.getInstance().createInternalCamera(OpenCvInternalCamera.CameraDirection.BACK, cameraMonitorViewId);
        webcam = OpenCvCameraFactory.getInstance().createWebcam(hardwareMap.get(WebcamName.class, "cameraMonitorViewId"), cameraMonitorViewId);

        pipeline = new VisionTestBB();
        webcam.setPipeline(pipeline);


        //int cameraMonitorViewId = hardwareMap.appContext.getResources().getIdentifier("cameraMonitorViewId", "id", hardwareMap.appContext.getPackageName());
        //webcam = OpenCvCameraFactory.getInstance().createWebcam(hardwareMap.get(WebcamName.class, "cameraMonitorViewId"), cameraMonitorViewId);

        //pipeline = new VisionTest1v2ducks.SamplePipeline();
        //webcam.setPipeline(pipeline);


        // We set the viewport policy to optimized view so the preview doesn't appear 90 deg
        // out when the RC activity is in portrait. We do our actual image processing assuming
        // landscape orientation, though.
        //webcam.setViewportRenderingPolicy(OpenCvCamera.ViewportRenderingPolicy.OPTIMIZE_VIEW);

        webcam.openCameraDeviceAsync(new OpenCvCamera.AsyncCameraOpenListener() {
            @Override
            public void onOpened() {
                webcam.startStreaming(320, 240, OpenCvCameraRotation.UPRIGHT);
            }
        });

        waitForStart();

        //Add Telemetry
        telemetry.addData("Value:", pipeline.position);
        //telemetry.addData("red_AverageD1_0", pipeline.getAverageD1());
        //telemetry.addData("red_AverageD2_0", pipeline.getAverageD2());
        //telemetry.addData("BLU_AverageD1", pipeline.getAverageD7());
        //telemetry.addData("BLU_AverageD2", pipeline.getAverageD8());
        telemetry.addData("Yellow_AverageD1", pipeline.getAverageD9());
        telemetry.addData("Yellow_AverageD2", pipeline.getAverageD10());
        telemetry.addData("Thres_AverageD1", pipeline.getAverageD11());
        telemetry.addData("Thres_AverageD2", pipeline.getAverageD12());
        telemetry.addData("ThresInv_AverageD1", pipeline.getAverageD13());
        telemetry.addData("ThresInv_AverageD2", pipeline.getAverageD14());
        telemetry.update();

        Pose2d startPose = new Pose2d(60.0, 12.0, Math.toRadians(0.0));
        Pose2d firstPose = new Pose2d(40.0, -12.0, Math.toRadians(0.0));
        Pose2d secondPose = new Pose2d(52.0, -5.0, Math.toRadians(0.0));
        Pose2d thirdPose = new Pose2d(48.0, -5.0, Math.toRadians(90.0));
        Pose2d forthPose = new Pose2d(36.0, 59.0, Math.toRadians(90.0));

        Trajectory builder1 = drive.trajectoryBuilder(startPose).back(1).build(); //forward
        Trajectory builder2 = drive.trajectoryBuilder(builder1.end()).lineToLinearHeading(firstPose).build();
        Trajectory builder3 = drive.trajectoryBuilder(builder2.end()).lineToLinearHeading(secondPose).build();
        Trajectory builder4 = drive.trajectoryBuilder(builder3.end()).lineToLinearHeading(thirdPose).build();
        Trajectory builder5 = drive.trajectoryBuilder(builder4.end()).lineToLinearHeading(forthPose).build();

        shootingSystem.autoFireRings();
        sleep(500);
        shootingSystem.stopFiring();
        LevelNumber = 0;

        if (pipeline.position == FFRedRight.VisionTestBB.RingPosition.DUCK2) {
            shootingSystem.autoFireRings();
            sleep(500);
            shootingSystem.stopFiring();
            LevelNumber = 1;
        }

        if (pipeline.position == FFRedRight.VisionTestBB.RingPosition.DUCK3) {
            shootingSystem.autoFireRings();
            sleep(500);
            shootingSystem.stopFiring();
            sleep(500);
            shootingSystem.autoFireRings();
            sleep(500);
            shootingSystem.stopFiring();
            LevelNumber = 2;
        }


        drive.setPoseEstimate(startPose); //id the initial start position
        sleep(500);
        drive.followTrajectory(builder1);
        sleep(500);
        drive.followTrajectory(builder2);
        sleep(500);
        shootingSystem.level0();
        sleep(500);

        //if (pipeline.position == VisionTestBB.RingPosition.DUCK2) {
        if (LevelNumber == 1) {
            shootingSystem.level1();
            sleep(500);
        }

        //if (pipeline.position == VisionTestBB.RingPosition.DUCK3) {
        if (LevelNumber == 2) {
            shootingSystem.level2();
            sleep(500);
        }


        shootingSystem.UnLoad();
        sleep(1500);
        shootingSystem.Load();
        sleep(100);

            //shootingSystem.level0();
            drive.followTrajectory(builder3);
            sleep(500);
            shootingSystem.level0();
            drive.followTrajectory(builder4);
            sleep(500);
            drive.followTrajectory(builder5);
            sleep(100);
            shootingSystem.level4();
    }
    public static class VisionTestBB extends OpenCvPipeline
    {



        private static final Scalar BLUE = new Scalar(0, 0, 255);
        private static final Scalar GREEN = new Scalar(0, 255, 0);
        private static final int THRESHOLD = 80;
        //Point topLeftD1 = new Point(50, 150);
        //Point bottomRightD1 = new Point(90, 190);
        //Point topLeftD2 = new Point(220, 150);
       // Point bottomRightD2 = new Point(260, 190);

        Point topLeftD1 = new Point(90, 150);
        Point bottomRightD1 = new Point(130, 190);
        Point topLeftD2 = new Point(260, 180);
        Point bottomRightD2 = new Point(300, 220);

        Mat region1_Crcol;
        Mat region2_Crcol;
        Mat region1_Cbcol;
        Mat region2_Cbcol;
        Mat region1_Ycol;
        Mat region2_Ycol;
        Mat region1_Thres;
        Mat region2_Thres;
        Mat region1_ThresInv;
        Mat region2_ThresInv;
        Mat YCrCb = new Mat();
        Mat Crcol = new Mat();
        Mat Ycol = new Mat();
        Mat Cbcol = new Mat();
        Mat tholdMat = new Mat(); // This will store the threshold
        Mat tholdMatinv = new Mat(); // This will store the threshold

        private volatile int averageD1;
        private volatile int averageD2;
        private volatile int averageD7;
        private volatile int averageD8;
        private volatile int averageD9;
        private volatile int averageD10;
        private volatile int averageD11;
        private volatile int averageD12;
        private volatile int averageD13;
        private volatile int averageD14;
        //private volatile TYPE type = TYPE.DUCK1;
        private volatile RingPosition position = RingPosition.DUCK1;


        private void inputToCb(Mat input) {
            Imgproc.cvtColor(input, YCrCb, Imgproc.COLOR_RGB2YCrCb);
            Core.extractChannel(YCrCb, Ycol, 0);
            Core.extractChannel(YCrCb, Crcol, 1);
            Core.extractChannel(YCrCb, Cbcol, 2);
            Imgproc.threshold(Ycol, tholdMat, 90, 255, Imgproc.THRESH_BINARY);
            Imgproc.threshold(Ycol, tholdMatinv, 150, 255, Imgproc.THRESH_BINARY_INV);
        }

        @Override
        public void init(Mat firstFrame) {
            inputToCb(firstFrame);

            region1_Crcol = Crcol.submat(new Rect(topLeftD1, bottomRightD1));
            region2_Crcol = Crcol.submat(new Rect(topLeftD2, bottomRightD2));
            region1_Cbcol = Cbcol.submat(new Rect(topLeftD1, bottomRightD1));
            region2_Cbcol = Cbcol.submat(new Rect(topLeftD2, bottomRightD2));
            region1_Ycol = Ycol.submat(new Rect(topLeftD1, bottomRightD1));
            region2_Ycol = Ycol.submat(new Rect(topLeftD2, bottomRightD2));
            region1_Thres = tholdMat.submat(new Rect(topLeftD1, bottomRightD1));
            region2_Thres = tholdMat.submat(new Rect(topLeftD2, bottomRightD2));
            region1_ThresInv = tholdMatinv.submat(new Rect(topLeftD1, bottomRightD1));
            region2_ThresInv = tholdMatinv.submat(new Rect(topLeftD2, bottomRightD2));

        }

        @Override
        public Mat processFrame(Mat input) {
            inputToCb(input);
            averageD1 = (int) Core.mean(region1_Crcol).val[0];
            averageD2 = (int) Core.mean(region2_Crcol).val[0];
            averageD7 = (int) Core.mean(region1_Cbcol).val[0];
            averageD8 = (int) Core.mean(region2_Cbcol).val[0];
            averageD9 = (int) Core.mean(region1_Ycol).val[0];
            averageD10 = (int) Core.mean(region2_Ycol).val[0];
            averageD11 = (int) Core.mean(region1_Thres).val[0];
            averageD12 = (int) Core.mean(region2_Thres).val[0];
            averageD13 = (int) Core.mean(region1_ThresInv).val[0];
            averageD14 = (int) Core.mean(region2_ThresInv).val[0];

            Imgproc.rectangle(input, topLeftD1, bottomRightD1, BLUE, 4);
            Imgproc.putText(input, "LEFT", topLeftD1, 1, 1.3, BLUE);
            Imgproc.rectangle(input, topLeftD2, bottomRightD2, BLUE, 2);
            Imgproc.putText(input, "CENTER", topLeftD1, 1, 1.3, BLUE);

            position = RingPosition.DUCK1; // Record our analysis
            if
            ((averageD9 + 30) < averageD10) {
                position = RingPosition.DUCK1;
                Imgproc.rectangle(input, topLeftD1, bottomRightD1, GREEN, 5);
            }
            //position = RingPosition.DUCK1;
            else if
            (averageD9 > (averageD10 + 30)) {
                position = RingPosition.DUCK2;
                Imgproc.rectangle(input, topLeftD2, bottomRightD2, GREEN, 5);
            }
            //position = RingPosition.DUCK3;
            else
                {position = RingPosition.DUCK3;}
            return input;
        }

        //public VisionTest1v2ducks.SamplePipeline.TYPE getType() {
        //    return type;
        //}}

        //public TYPE getType() {return type;}
        public int getAverageD1() {return averageD1;
        }
        public int getAverageD2() {return averageD2;
        }
        public int getAverageD7() {return averageD7;
        }
        public int getAverageD8() {return averageD8;
        }
        public int getAverageD9() {return averageD9;
        }
        public int getAverageD10() {return averageD10;
        }
        public int getAverageD11() {return averageD11;
        }
        public int getAverageD12() {return averageD12;
        }
        public int getAverageD13() {return averageD13;
        }
        public int getAverageD14() {return averageD14;
        }
       // public int getAnalysis()
        public enum RingPosition {
            DUCK1, DUCK2, DUCK3
        }

    }
}