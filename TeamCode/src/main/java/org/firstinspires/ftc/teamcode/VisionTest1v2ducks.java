package org.firstinspires.ftc.teamcode;

//package org.firstinspires.ftc.teamcode.auto.test;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

import org.firstinspires.ftc.robotcore.external.hardware.camera.WebcamName;
import org.opencv.core.Core;
import java.util.ArrayList; //added for array
import java.util.List; //added for poly list
import org.opencv.core.MatOfByte; // dded for list of points for poly
import org.opencv.core.MatOfPoint; //added for list of points for poly
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import org.openftc.easyopencv.OpenCvCamera;
import org.openftc.easyopencv.OpenCvCameraFactory;
import org.openftc.easyopencv.OpenCvCameraRotation;
import org.openftc.easyopencv.OpenCvPipeline;

//@TeleOp
@com.qualcomm.robotcore.eventloop.opmode.Autonomous
//Ball or Cube

public class VisionTest1v2ducks extends LinearOpMode {
    OpenCvCamera webcam;
    //charlie code


    //misc note

    SamplePipeline pipeline;

    @Override
    public void runOpMode() {
        int cameraMonitorViewId = hardwareMap.appContext.getResources().getIdentifier("cameraMonitorViewId", "id", hardwareMap.appContext.getPackageName());
        webcam = OpenCvCameraFactory.getInstance().createWebcam(hardwareMap.get(WebcamName.class, "cameraMonitorViewId"), cameraMonitorViewId);

        pipeline = new SamplePipeline();
        webcam.setPipeline(pipeline);


        webcam.openCameraDeviceAsync(new OpenCvCamera.AsyncCameraOpenListener()
        {
            @Override
            public void onOpened()
            {
                webcam.startStreaming(320, 240, OpenCvCameraRotation.UPRIGHT);
            }

           // catch(err) { isOk= false; }
           // if(isOk) telemetry.addData("Ok");
           // else telemetry.addData("ERROR");
           // telemetry.update();


            //@Override
            // public void onError(int errorCode){}

        });
        waitForStart();

        while (opModeIsActive()) {
            telemetry.addData("Type", pipeline.getType());
            telemetry.addData("red_AverageD1_0", pipeline.getAverageD1());
            telemetry.addData("red_AverageD2_0", pipeline.getAverageD2());
            telemetry.addData("BLU_AverageD1", pipeline.getAverageD7());
            telemetry.addData("BLU_AverageD2", pipeline.getAverageD8());
            telemetry.addData("Yellow_AverageD1", pipeline.getAverageD9());
            telemetry.addData("Yellow_AverageD2", pipeline.getAverageD10());
            telemetry.addData("Thres_AverageD1", pipeline.getAverageD11());
            telemetry.addData("Thres_AverageD2", pipeline.getAverageD12());
            telemetry.update();
            sleep(50);
        }
    }

    public static class SamplePipeline extends OpenCvPipeline {
        private static final Scalar BLUE = new Scalar(0, 0, 255);
        private static final Scalar GREEN = new Scalar(0, 255, 0); // RGB values for green.
        private static final Scalar RED = new Scalar(255, 0, 0);

        private static final int THRESHOLD = 100;

        Point topLeftD1 = new Point(100, 180);
        Point bottomRightD1 = new Point(130, 210);
        Point topLeftD2 = new Point(280, 180);
        Point bottomRightD2 = new Point(310, 210);

        Mat region1_Crcol;
        Mat region2_Crcol;
        Mat region1_Cbcol;
        Mat region2_Cbcol;
        Mat region1_Ycol;
        Mat region2_Ycol;
        Mat region1_Thres;
        Mat region2_Thres;
        Mat YCrCb = new Mat();
        Mat Crcol = new Mat();
        Mat Ycol = new Mat();
        Mat Cbcol = new Mat();
        Mat tholdMat = new Mat(); // This will store the threshold
        Mat tholdMatinv = new Mat(); // This will store the threshold

        private volatile int averageD1;
        private volatile int averageD2;
        private volatile int averageD7;
        private volatile int averageD8;
        private volatile int averageD9;
        private volatile int averageD10;
        private volatile int averageD11;
        private volatile int averageD12;
        private volatile TYPE type = TYPE.DUCK1;

        private void inputToCb(Mat input) {
            Imgproc.cvtColor(input, YCrCb, Imgproc.COLOR_RGB2YCrCb);
            Core.extractChannel(YCrCb, Ycol, 0);
            Core.extractChannel(YCrCb, Crcol, 1);
            Core.extractChannel(YCrCb, Cbcol, 2);
            //Imgproc.threshold(Crcol, tholdMat, 90, 255, Imgproc.THRESH_BINARY_INV);
            //Imgproc.threshold(Ycol, tholdMat, 142, 255, Imgproc.THRESH_BINARY);
            Imgproc.threshold(Ycol, tholdMat, 142, 250, Imgproc.THRESH_BINARY);
            Imgproc.threshold(Ycol, tholdMatinv, 150, 255, Imgproc.THRESH_BINARY_INV);
            //region1_Cb = tholdMat.submat(new Rect(topLeftD1, bottomRightD1));
            //region2_Cb = tholdMat.submat(new Rect(topLeftD2, bottomRightD2));
        }

            // Img processing
            //  Imgproc.cvtColor(input, YCrCb, Imgproc.COLOR_BGR2YCrCb);
            //  Core.extractChannel(YCrCb, Cb, 2);


        @Override
        public void init(Mat input) {
            inputToCb(input);

            //region1_Cb = tholdMat.submat(new Rect(topLeftD1, bottomRightD1));
            //region2_Cb = tholdMat.submat(new Rect(topLeftD2, bottomRightD2));
            region1_Crcol = Crcol.submat(new Rect(topLeftD1, bottomRightD1));// Crcol= ChromaRed
            region2_Crcol = Crcol.submat(new Rect(topLeftD2, bottomRightD2));
            region1_Cbcol = Cbcol.submat(new Rect(topLeftD1, bottomRightD1)); //Cb2 = Chroma Blue
            region2_Cbcol = Cbcol.submat(new Rect(topLeftD2, bottomRightD2));
            region1_Ycol = Ycol.submat(new Rect(topLeftD1, bottomRightD1));
            region2_Ycol = Ycol.submat(new Rect(topLeftD2, bottomRightD2));
            region1_Thres = tholdMat.submat(new Rect(topLeftD1, bottomRightD1));
            region2_Thres = tholdMat.submat(new Rect(topLeftD2, bottomRightD2));
            //double[] leftSquarePointValues = tholdMat.get((topLeftD1.x), (bottomRightD1.y));
            //double[] rightSquarePointValues = tholdMat.get((topLeftD2.x), (bottomRightD2.y));
            //region1_Cb = tholdMat.submat(new Rect(topLeftD1, bottomRightD1));
            //region2_Cb = tholdMat.submat(new Rect(topLeftD2, bottomRightD2));


        }

        @Override
        public Mat processFrame(Mat input) {
            inputToCb(input);
                //region2_Ycol, region2_Cb2
           averageD1 = (int) Core.mean(region1_Crcol).val[0];
           averageD2 = (int) Core.mean(region2_Crcol).val[0];
           averageD7 = (int) Core.mean(region1_Cbcol).val[0];
           averageD8 = (int) Core.mean(region2_Cbcol).val[0];
           averageD9 = (int) Core.mean(region1_Ycol).val[0];
           averageD10 = (int) Core.mean(region2_Ycol).val[0];
           averageD11 = (int) Core.mean(region1_Thres).val[0];
           averageD12 = (int) Core.mean(region2_Thres).val[0];


            //averageD1 = (int) leftSquarePointValues[0];
            //averageD2 = (int) rightSquarePointValues[0];

            Imgproc.rectangle(input, topLeftD1, bottomRightD1, BLUE, 2);
            Imgproc.putText(input, "LEFT", topLeftD1, 1,1.3, BLUE);
            Imgproc.rectangle(input, topLeftD2, bottomRightD2, BLUE, 2);
            Imgproc.putText(input, "CENTER", topLeftD2, 1,1.3, BLUE);

            // Drawing a line
            //Imgproc.line (
                    //input,                    // input obj of the image
                    //new Point(10, 200),        //p1
                    //new Point(300, 200),       //p2
                    //new Scalar(0, 0, 255),     //Scalar object for color
                    //5                          //Thickness of the line
            //);

            // list of poly lines points
            List<MatOfPoint> list2 = new ArrayList();
            list2.add(
                    new MatOfPoint (
                            new Point(60, 240), new Point(110, 110),
                            new Point(160, 240)
                    )
            );
            // list of poly lines points
            List<MatOfPoint> list3 = new ArrayList();
            list3.add(
                    new MatOfPoint (
                            new Point(240, 240), new Point(290, 130),
                            new Point(320, 180)
                    )
            );
            // Drawing polylines
            Imgproc.polylines (
                    input,                    // input obj of the image
                    list2,                      // java.util.List<MatOfPoint> pts
                    false,                     // isClosed
                    RED,     // Scalar object for color
                    5                          // Thickness of the line
            );
            // Drawing polylines
            Imgproc.polylines (
                    input,                    // input obj of the image
                    list3,                      // java.util.List<MatOfPoint> pts
                    false,                     // isClosed
                    RED,     // Scalar object for color
                    5                          // Thickness of the line
            );


            //Imgproc.minEnclosingTriangle()

            //if
            //(averageD9 < THRESHOLD) {type = TYPE.DUCK2;}
            //else if
            //(averageD10 < THRESHOLD)  {type = TYPE.DUCK3;}
            //else {type = TYPE.DUCK1;}
            //return input;

            // Change colors if the pipeline detected something



            if
            //((averageD9 +20) <averageD10)
            ((averageD11 - 40) > averageD12)

            {type = TYPE.DUCK1;
                Imgproc.rectangle(input, topLeftD1, bottomRightD1, GREEN, 5);
            }

            else if
            //(averageD9 >(averageD10+20))
            (averageD11 < (averageD12 - 40))

            {type = TYPE.DUCK2;
                Imgproc.rectangle(input, topLeftD2, bottomRightD2, GREEN, 5);

            }
                        else
            {type = TYPE.DUCK3;
            }
            return input;



        }

        public TYPE getType() {
            return type;
        }

        public int getAverageD1() {
            return averageD1;
        }
        public int getAverageD2() {
            return averageD2;
        }
        public int getAverageD7() {
            return averageD7;
        }
        public int getAverageD8() {
            return averageD8;
        }
        public int getAverageD9() {
            return averageD9;
        }
        public int getAverageD10() {
            return averageD10;
        }
        public int getAverageD11() {
            return averageD11;
        }
        public int getAverageD12() {
            return averageD12;
        }

        public enum TYPE {
            DUCK1, DUCK2, DUCK3
        }
    }
}
