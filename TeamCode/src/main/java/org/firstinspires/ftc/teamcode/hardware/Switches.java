package org.firstinspires.ftc.teamcode.hardware;

import com.qualcomm.robotcore.hardware.DigitalChannel;
import com.qualcomm.robotcore.hardware.HardwareMap;

public class Switches {
    private DigitalChannel
            SwitchAllianceBlue,
            SwitchAllianceRed,
            SwitchStartInner,
            SwitchStartOuter;
    boolean SABState; boolean SARState;
    boolean SSLState; boolean SSRState;
    public Switches(HardwareMap hardwareMap) {

        SwitchAllianceBlue = hardwareMap.get(DigitalChannel.class, "SwitchAllianceBlue");
        SwitchAllianceRed = hardwareMap.get(DigitalChannel.class, "SwitchAllianceRed");
        SwitchStartInner = hardwareMap.get(DigitalChannel.class, "SwitchStartInner");
        SwitchStartOuter = hardwareMap.get(DigitalChannel.class, "SwitchStartOuter");

        SwitchAllianceBlue.setMode(DigitalChannel.Mode.INPUT);
        SwitchStartInner.setMode(DigitalChannel.Mode.INPUT);
    }
    public boolean[] readSwitches(){
        SwitchAllianceBlue.setMode(DigitalChannel.Mode.INPUT);
        SwitchAllianceRed.setMode(DigitalChannel.Mode.INPUT);
        SwitchStartInner.setMode(DigitalChannel.Mode.INPUT);;
        SABState = SwitchAllianceBlue.getState(); SARState = SwitchAllianceRed.getState();
        SSLState = SwitchStartInner.getState(); SSRState = SwitchStartOuter.getState();
        boolean[] switchStates = {SABState, SSLState, SARState, SSRState};
        return switchStates;
    }
}